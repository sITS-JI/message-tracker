﻿Param(
	[string[]]$Servers,
	[string]$ResultSize = 'unlimited',
	[string]$Sender,
	[string[]]$Recipients,
	[string]$MessageSubject,
	[string]$MessageId,
	[string]$EventId,
	[string]$Start,
	[string]$End,
	[string]$CsvOutput,
	[string]$CsvDelimiter = ';'
)

$CmdArgs = @{
	ResultSize = $ResultSize
}

if ($Sender) {
	$CmdArgs['Sender'] = $Sender
}
if ($Recipients) {
	$CmdArgs['Recipients'] = $Recipients
}
if ($MessageSubject) {
	$CmdArgs['MessageSubject'] = $MessageSubject
}
if ($MessageId) {
	$CmdArgs['MessageId'] = $MessageId
}
if ($EventId) {
	$CmdArgs['EventId'] = $EventId
}
if ($Start) {
	$CmdArgs['Start'] = $Start
}
if ($End) {
	$CmdArgs['End'] = $End
}

if (!$Servers) {
	if (!(Get-ADServerSettings).ViewEntireForest) {
		Set-ADServerSettings -ViewEntireForest $true
	}
	$Servers = Get-TransportServer
}

$msg = $Servers | ForEach-Object { Get-MessageTrackingLog -Server $_ @CmdArgs }
if ($CsvOutput) {
	$trash = $msg `
		| Select-Object -Property * -ExcludeProperty PSComputerName, RunspaceId, PSShowComputerName, `
			SourceContext, MessageInfo, MessageLatency, MessageLatencyType, EventData `
		| Export-Csv $CsvOutput -NoTypeInformation -Encoding 'utf8' -Delimiter $CsvDelimiter
	start $CsvOutput
}
else {
	$msg
}